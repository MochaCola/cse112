open Absyn 

let want_dump = ref false

let rec eval_expr (expr : Absyn.expr) : float = match expr with
    | Number number -> number
    | Memref memref -> eval_memref memref
    | Unary (oper, expr) -> 
        (*Finds operation in unary table and with recursion finds
        The value the operator will be used on *) 
        let operfn = Hashtbl.find Tables.unary_fn_table oper and 
        val1 = eval_expr (expr) in
            operfn val1 
    | Binary (oper, expr1, expr2) ->
        (*Finds operation in binary table and with recursion finds
        The value the operator will be used on *)
        (try let operfn = Hashtbl.find Tables.binary_fn_table oper and
                 val1 = eval_expr(expr1) and val2 = eval_expr(expr2) in
                 operfn val1 val2;
         with Not_found -> print_string Sys.argv.(1);
                           Dumper.eval_error_helper oper;
                           0.0)

(*This is a special function called by interp_if for evaluating
boolean expressions *)
and eval_relexpr (relexpr : Absyn.relexpr) : bool = match relexpr with 
    | Relexpr (relexpr, expr1, expr2) ->
        let inequality = Hashtbl.find Tables.bool_fn_table relexpr and
        val1 = eval_expr (expr1) and val2 = eval_expr (expr2) in 
            inequality val1 val2
    
and eval_memref (memref : Absyn.memref) : float = match memref with
    | Arrayref (ident, expr) ->  
        (try let arr = Hashtbl.find Tables.array_table ident  in
            Array.get arr (Etc.int_of_round_float (eval_expr expr));
        with Not_found -> 0.0)
    | Variable ident -> 
        try Hashtbl.find Tables.variable_table ident
        with Not_found -> 0.0

let rec interpret (program : Absyn.program) = match program with
    | [] -> ()
    | firstline::continue -> match firstline with
      | linenumber, _, None -> interpret continue
      | linenumber, _, Some stmt -> (interp_stmt stmt continue)

and interp_stmt (stmt : Absyn.stmt) (continue : Absyn.program) =
    match stmt with
    | Dim (ident, expr) -> interp_dim ident expr continue
    | Let (memref, expr) -> interp_let memref expr continue
    | Goto label -> interp_goto label continue
    | If (expr, label) -> interp_if expr label continue
    | Print print_list -> interp_print print_list continue
    | Input memref_list -> interp_input memref_list continue

and interp_let (memref : Absyn.memref) (mainexpr : Absyn.expr)
                (continue : Absyn.program) = 
    (*Looks similar to eval_memref but not returning a float*)
    match memref with 
        | Arrayref (ident, expr) ->
            (*Array.set updates the array in the array_table*) 
            (try let arr = Hashtbl.find Tables.array_table ident in 
                Array.set arr (Etc.int_of_round_float
                (eval_expr expr)) (eval_expr mainexpr);
                interpret continue
            with Not_found -> 
                (*There are new functions in dumper.ml with correct
                error messages*)
                (*<progname> <linenumber> <function> <error> *)
                print_string Sys.argv.(1);
                Dumper.let_error_helper continue;
                ) 
        | Variable ident ->
            (*Evaluates the expression after "let" and stores the
            expression and variable in the variable table*)
            let eval = eval_expr (mainexpr) in 
                Hashtbl.add Tables.variable_table ident eval;
                interpret continue

and interp_goto (label : Absyn.label) (continue : Absyn.program) =
    (*Trys to find label in label table and passes it to the 
    interpret function, if label is not found an error occurs*)
    try let foundLabel = Hashtbl.find Tables.label_table label in
        interpret foundLabel;
    with Not_found -> 
        print_string Sys.argv.(1);
        Dumper.goto_error_helper continue; 


and interp_if (relexpr : Absyn.relexpr) (label : Absyn.label)
              (continue : Absyn.program) =
    let exprCorrect = eval_relexpr (relexpr) in
        (*Jumps to label if the expression is true, else
        the program continues without an error to the next line*)
        if exprCorrect then interp_goto label continue
        else interpret continue


and interp_print (print_list : Absyn.printable list)
                 (continue : Absyn.program) =
    let print_item item =
        match item with
        | String string ->
          let regex = Str.regexp "\"\\(.*\\)\""
          in print_string (Str.replace_first regex "\\1" string)
        | Printexpr expr ->
          print_string " "; print_float (eval_expr expr)
    in (List.iter print_item print_list; print_newline ());
    interpret continue


and interp_input (memref_list : Absyn.memref list)
                 (continue : Absyn.program)  =
    let input_number memref =
        try  let number = Etc.read_number ()
             in match memref with 
                | Variable indent ->
                    Hashtbl.add Tables.variable_table indent number;
                | Arrayref (ident, expr) -> 
                    (try let arr = Hashtbl.find Tables.array_table ident
                    and
                    index = (Etc.int_of_round_float (eval_expr expr)) in
                        Array.set arr index number;
                    with Not_found -> 
                        print_string Sys.argv.(1);
                        Dumper.input_error_helper (continue);)
        with End_of_file -> 
            (*End of file is reached and updated in variable table *)
            Hashtbl.add Tables.variable_table "eof" 1.; 
    in List.iter input_number memref_list;
    interpret continue

and interp_dim (ident : Absyn.ident) (expr : Absyn.expr)
               (continue : Absyn.program) = 
    let newArray = Array.make (Etc.int_of_round_float 
                   (eval_expr expr)) 0. in
        Hashtbl.add Tables.array_table ident newArray;
    interpret continue

let interpret_program program =
    (Tables.init_label_table program; 
     if !want_dump then Tables.dump_label_table ();
     if !want_dump then Dumper.dump_program program;
     interpret program)

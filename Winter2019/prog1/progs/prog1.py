#! /usr/bin/env python
import os.path
from subprocess import call
import sys

student = sys.argv[1]
score = 0

def parseExprTest(tnum,expr,points):
    testfile = "testout"+tnum+".txt"
    if (call(["ghc", "-e", 'Main.parseExpr  '+expr, "tlix.hs"],stdout=open(testfile,"w+")) == 0):
        if (not os.path.isfile(testfile)):
            print "No "+testfile+" generated"
        else:
            if (call(["diff","-b", "-B", "../../"+testfile, testfile]) == 0):
                print "correct "+testfile
                return points
            else: 
                print "wrong " + testfile
                return 0
    else:
        print "test"+tnum+" failed"
        return 0


def parseTest(tnum,expr,points):
    testfile = "testout"+tnum+".txt"
    if (call(["ghc", "-e", 'fst (Main.parseTest  '+expr+ " [])", "tlix.hs"],stdout=open(testfile,"w+")) == 0):
        if (not os.path.isfile(testfile)):
            print "No "+testfile+" generated"
        else:
            if (call(["diff","-b", "-B", "../../"+testfile, testfile]) == 0):
                print "correct "+testfile
                return points
            else: 
                print "wrong " + testfile
                return 0
    else:
        print "test"+tnum+" failed"
        return 0


#! require ST to also be correct
def parseTestST(tnum,expr,points):
    testfile = "testout"+tnum+".txt"
    if (call(["ghc", "-e", 'Main.parseTest  '+expr+ " []", "tlix.hs"],stdout=open(testfile,"w+")) == 0):
        if (not os.path.isfile(testfile)):
            print "No "+testfile+" generated"
        else:
            if (call(["diff","-b", "-B", "../../"+testfile, testfile]) == 0):
                print "correct "+testfile
                return points
            else: 
                print "wrong " + testfile
                return 0
    else:
        print "test"+tnum+" failed"
        return 0

def progTest(tnum,points):
    testinfile = "testin"+tnum+".txt"
    testoutfile = "testout"+tnum+".txt"
    progfile = "prog"+tnum+".txt"
    if (call(["tlix",  "../../"+progfile],stdin=open("../../"+testinfile,"r+"),stdout=open(testoutfile,"w+")) == 0):
        if (not os.path.isfile(testoutfile)):
            print "No "+testoutfile+" generated"
        else:
            if (call(["diff","-b", "-B", "../../"+testoutfile, testoutfile]) == 0):
                print "correct "+testoutfile
                return points
            else: 
                print "wrong " + testoutfile
                return 0
    else:
        print "test"+tnum+" failed"
        return 0


def progTestError(tnum,points):
    testinfile = "testin"+tnum+".txt"
    testoutfile = "testout"+tnum+".txt"
    progfile = "prog"+tnum+".txt"
    if (call(["tlix",  "../../"+progfile],stdin=open("../../"+testinfile,"r+"),stderr=open(testoutfile,"w+")) != 0):
        if (not os.path.isfile(testoutfile)):
            print "No "+testoutfile+" generated"
        else:
            if (call(["diff","-b", "-B", "../../"+testoutfile, testoutfile]) == 0):
                print "correct "+testoutfile
                return points
            else: 
                print "wrong " + testoutfile
                return 0
    else:
        print "test"+tnum+" failed"
        return 0


if (not os.path.isdir(student)):
    print student,"not a directory"
    sys.exit(1)
else:
    print "processing",student
    os.chdir(student)

if (not os.path.isfile('tli.hs')):
    print "No tli.hs"
else: print "found tli.hs"

call(["rm","tlix","testout*.txt"])

if (call(["cat", "../../header.hs","tli.hs"],stdout=open("tlix.hs","w+")) == 0):
    print "succesfully created modified tlix.hs"
else:
    print "failed to created modified tlix.hs"
    sys.exit(1)

if (call(["ghc", "--make", "tlix.hs"]) == 0):
    if (os.path.isfile('tlix')): score = score + 1
    else: print "no executable tlix created"
else:
    print "compile failed"


score = score + parseExprTest("1a", '["this", "*", "that"]',1)
score = score + parseExprTest("1b", '["x", "/", "2.2"]',1)
score = score + parseTest("2a", '[["if","a","<","b","goto","loop"],["let","x","=","a","*","bbb"],["print","a",",","b"]]',1)
score = score + parseTestST("3a", '[["if","a","<","b","goto","loop"],["let","x","=","a","*","bbb"],["loop:","print","a",",","b"]]',1)
score = score + progTest("4a",1)
score = score + progTest("5a",1)
score = score + progTest("6a",1)
score = score + progTestError("7a",1)

print student, "score is ", score

call(["rm","tlix.hs"])

